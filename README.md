# YG-App

#### 拉取镜像
```pgyniub/vdai:latest```

#### 配置环境变量

```
SSencryption = chacha20-ietf-poly1305
UUID = 你的UUID
```

#### 客户端配置如下

IOS端小火箭就可以通吃，安卓端推荐V2rayNG或搭配Kitsunebi

协议：(vless/vmess/trojan)-ws

地址：app.heroku.com（自选IP/域名）

端口：443

用户ID/密码：自定义UUID

传输协议：ws

伪装host：app.heroku.com（workers或pages反代/自定义域）

路径path：/自定义UUID-协议开头两小写字母

传输安全：tls

SNI：app.heroku.com（workers或pages反代/自定义域）

其他设置保持默认即可！



shadowsocks-ws与socks5-ws推荐用Kitsunebi，配置简单，不需要plugin插件

协议：(shadowsocks/socks5)-ws

地址：app.heroku.com（自选IP/域名）

端口：443

shadowsocks密码：自定义UUID

shadowsocks加密方式：chacha20-ietf-poly1305(默认)

socks5用户名：空

socks5密码：空

传输协议：ws

伪装host：app.heroku.com（workers或pages反代/自定义域）

路径path：/自定义UUID-协议开头两小写字母

传输安全：tls

SNI(证书域名)：app.heroku.com（workers或pages反代/自定义域）

其他设置保持默认即可！
